#!/root/.rbenv/shims/ruby

require 'json'

ARGF.each_line do |line|
	cleaned_line = line.sub(/[^}]*$/, '')
	puts JSON.parse(cleaned_line).inspect
end
